package de.rtcleaners.zeiterfassung;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class GetData {
    /*
    Utility uses the log in data to get current tasks.
    Connects to server, pulls the tasks, and stores them locally.
    Steps:
    1. TODO: Check if Login data is saved if not fail and goto login activity
    2. Check Online if not goto 6 and podcast no internet connection
    3. TODO: Log In
    4. Get Options
    5. Save Options
    6. Check if Options saved locally if not FAIL (Nothing local no internet)
    7. Return Options
    */
    private final String PREFERENCE_FILE_KEY_OPTIONS = "OPTIONS_STORE";
    private final String TASK_TEXTS = "TASK_TEXTS";

    //Load saved data
    public Options get_local_data(Context context){
        SharedPreferences storage = context.getSharedPreferences(
                PREFERENCE_FILE_KEY_OPTIONS,
                Context.MODE_PRIVATE
        );
        Set<String> res = storage.getStringSet(TASK_TEXTS, null);
        if(res == null){
            return new Options();
        }
        return new Options(res);
    }
    //Get the task data from the server
    public boolean get_data(Context context) {
        String online_data_0 = get_online_data(context);
        if (online_data_0 == null || online_data_0.length() < 5) {
            return false;
        }
        String online_data_1 = online_data_0.replace("\n", "").replace("\r", "");;
        if (online_data_1 == null || online_data_1.length() < 5) {
            return false;
        }
        String[] online_data_2 = online_data_1.split(";");
        Set<String> final_data = new HashSet<String>();
        for (int i=0; i < online_data_2.length; i++) {
            //TODO: Full proof that string line is a fully valid Option
            if (online_data_2 == null || online_data_2[i].length() < 5) {
                continue;
            }
            final_data.add(online_data_2[i]);
        }
        if (final_data.size() < 2) {
            //Assume that every task list must contain at least two tasks to make sense
            //Because a single task is equivalent to the default/unknown task
            return false;
        }
        save_data(final_data, context);
        return true;
    }

    //Log in and get data from server
    private String get_online_data(Context context) {
        HttpURLConnection urlConnection = null;
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL("https://rtcleaners.de/arbeit/taskxml");//+get_login_data(context));
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");

            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
    //Save pulled data
    private boolean save_data(Set<String> input, Context context) {
        SharedPreferences storage = context.getSharedPreferences(
                PREFERENCE_FILE_KEY_OPTIONS,
                Context.MODE_PRIVATE
        );
        return storage.edit().putStringSet(TASK_TEXTS, input).commit();
    }
}


