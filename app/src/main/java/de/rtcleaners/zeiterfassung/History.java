package de.rtcleaners.zeiterfassung;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.HashSet;
import java.util.Set;

public class History extends AppCompatActivity {

    public static final String PREFERENCE_HISTORY_OF_TASK = "PREFERENCE_HISTORY_OF_TASK";
    public static final String TASK_OF_HISTORY = "TASK_OF_HISTORY";

    public static void save_data(Context context, String name, int hours, int minutes) {
        SharedPreferences storageHistory = context.getSharedPreferences(
                History.PREFERENCE_HISTORY_OF_TASK,
                Context.MODE_PRIVATE
        );
        String old_string = storageHistory.getString(History.TASK_OF_HISTORY, "No History yet");

        String new_string = old_string + "\n" + name + " " + hours + " h " + minutes + " min.";
        storageHistory.edit().putString(History.TASK_OF_HISTORY, new_string).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        SharedPreferences storageHistory = getSharedPreferences(
                PREFERENCE_HISTORY_OF_TASK,
                Context.MODE_PRIVATE
        );
        String text = storageHistory.getString(TASK_OF_HISTORY, "");
        ((TextView)findViewById(R.id.maintextview)).setText(text);
    }

}