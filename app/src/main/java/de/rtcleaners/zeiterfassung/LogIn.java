package de.rtcleaners.zeiterfassung;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

public class LogIn extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
    public void onclicklogin(View v)
    {
        Log.i("RTCleanerinfo", "Save login data. Is not null now...");
        String user_text = ((EditText) findViewById(R.id.username)).getText().toString();
        String passw_text = ((EditText) findViewById(R.id.keyname)).getText().toString();
        SharedPreferences storage = this.getSharedPreferences(
                MainActivity.PREFERENCE_FILE_KEY_LOG_IN,
                Context.MODE_PRIVATE
        );
        storage.edit().putString(MainActivity.LOG_IN_USER, user_text).commit();
        storage.edit().putString(MainActivity.LOG_IN_PASSW, passw_text).commit();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
