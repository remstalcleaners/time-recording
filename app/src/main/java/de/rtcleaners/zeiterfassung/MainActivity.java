package de.rtcleaners.zeiterfassung;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    //Login Data Storage
    public static final String PREFERENCE_FILE_KEY_LOG_IN = "LOG_IN_DATA_STORE";
    public static final String LOG_IN_USER  = "LOG_IN_USER";
    public static final String LOG_IN_PASSW = "LOG_IN_PASSW";

    public static final String No_USER_FOUND    = "NoUserFound";
    public static final String No_TASK_ID_FOUND = "NoTaskIdFound";

    //Storage of current task, started working or stopped working etc.
    private static final String PREFERENCE_ACTIVE_TASK = "ACTIVE_TASK_INFO";
    private static final String ACTIVE_STATE = "ACTIVE_STATE";
    private static final String ACTIVE_TASK  = "ACTIVE_TASK";
    private static final String ACTIVE_NAME  = "ACTIVE_NAME";
    private static final String ACTIVE_TIME  = "ACTIVE_TIME";

    //The global Task list. It contains the currently collected tasks in memory.
    Options task_list = new Options(); //Must never be null or zero length
    //Initialization ###################################################################################################
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Verify that the app has a user set and the password stored
        SharedPreferences storageUser = this.getSharedPreferences(
                PREFERENCE_FILE_KEY_LOG_IN,
                Context.MODE_PRIVATE
        );
        String user = storageUser.getString(LOG_IN_USER, null);
        String password = storageUser.getString(LOG_IN_PASSW, null);
        if (user == null || password == null) {
            Intent intent = new Intent(this, LogIn.class);
            startActivity(intent);
            return;
        }
        //Pull data from server
        get_online_data(this, user, password);
        //Load the task data
        SharedPreferences storage = this.getSharedPreferences(
                PREFERENCE_ACTIVE_TASK,
                Context.MODE_PRIVATE
        );
        long task_time = storage.getLong(ACTIVE_TIME, 0);
        boolean state = storage.getBoolean(ACTIVE_STATE,false);
        //Init Timer System
        cut = new CountUpTimer(1000);
        if(state) {
            cut.start(task_time);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        //Immediately load locally stored data
        get_local_data();
        //Update Graphics
        update_spinner();
        create_display();
    }
    //Change Graphics ##################################################################################################
    //Update the spinner by taking the current data from the global Option list.
    private void update_spinner() {
        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.task_spinner);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        List<String> categories = new ArrayList<String>();
        for(int i = 0; i < task_list.options_list.length; i++) {
            categories.add(task_list.options_list[i].task_name + " (" + task_list.options_list[i].task_id + ")");
        }
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }
    private void create_display() {
        SharedPreferences storage = this.getSharedPreferences(
                PREFERENCE_ACTIVE_TASK,
                Context.MODE_PRIVATE
        );
        boolean state = storage.getBoolean(ACTIVE_STATE,false);
        String task_id = storage.getString(ACTIVE_TASK,"");
        String task_name = storage.getString(ACTIVE_NAME,"");
        if (state) {
            //Work started. No options available etc.
            findViewById(R.id.task_spinner).setVisibility(View.INVISIBLE);
            ((Button) findViewById(R.id.button_main)).setText("Arbeit ausstempeln");
            ((Button) findViewById(R.id.button_main)).setBackgroundColor(0xFFFF0000);
            ((TextView) findViewById(R.id.instruction_view)).setText("Jetzt läuft:\n" + task_name + " (" + task_id + ")");
        } else {
            //Work finished. All visible again etc.
            findViewById(R.id.task_spinner).setVisibility(View.VISIBLE);
            ((Button) findViewById(R.id.button_main)).setText("Arbeit einstempeln");
            ((Button) findViewById(R.id.button_main)).setBackgroundColor(0xFF0071BC);
            ((TextView) findViewById(R.id.instruction_view)).setText("Bitte auswählen und einstempeln");
        }
    }
    //Get Data #########################################################################################################
    //Get locally stored data if available
    private void get_local_data() {
        GetData getter = new GetData();
        Options res = getter.get_local_data(this);
        if (res == null) {
            return;
        }
        this.task_list = res;
    }
    private static boolean updating = false;
    //Get Data function
    public static void get_online_data(Context context, String user, String password) {
        if(!MainActivity.updating) {
            MainActivity.updating = true;
        Toast toast = Toast.makeText(context, "Versuche Daten zu empfangen...", Toast.LENGTH_SHORT);
        toast.show();
        //Run on separate thread to not lock the main thread.
        new Thread(new Runnable() {
            public void run() {
                GetData getter = new GetData();
                boolean worked = getter.get_data(context);
                updating = false;
                //Send error message, offline, on the main thread.
                Handler refresh = new Handler(Looper.getMainLooper());
                refresh.post(new Runnable() {
                    public void run()
                    {
                        String message = "Fehler: Update fehlgeschlagen!";
                        if (worked) {
                            message = "Daten aktualisiert!";
                        }
                        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
            }
        }).start();
        }
    }
    //Buttons ##########################################################################################################
    //Button to get data again
    public void onclick_open_menu(View v) {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
    //Change work state button #########################################################################################
    private long delayer = 0;
    public void onclick_main_button(View v) {
        long time_now = System.currentTimeMillis();
        if (delayer - time_now > 0){
            return;
        }
        delayer = time_now + 2000;
        SharedPreferences storage = this.getSharedPreferences(
                PREFERENCE_ACTIVE_TASK,
                Context.MODE_PRIVATE
        );
        boolean state = storage.getBoolean(ACTIVE_STATE,false);
        if(state) {//True = in work mode, so stop work mode
            stop_work();
        }else{//False = not in work mode, switch to work mode
            start_work();
        }
        create_display();
    }
    private void start_work() {
        //Save the selected task_id locally (so App can be closed)
        SharedPreferences storage = this.getSharedPreferences(
                PREFERENCE_ACTIVE_TASK,
                Context.MODE_PRIVATE
        );
        Spinner spinner = (Spinner) findViewById(R.id.task_spinner);
        Options.Option selected_task = task_list.options_list[spinner.getSelectedItemPosition()];
        String selected_task_id = selected_task.task_id;
        String selected_task_name = selected_task.task_name;

        storage.edit().putString(ACTIVE_TASK, selected_task_id).commit();
        storage.edit().putString(ACTIVE_NAME, selected_task_name).commit();
        storage.edit().putBoolean(ACTIVE_STATE, true).commit();
        long time_now =  System.currentTimeMillis();
        storage.edit().putLong(ACTIVE_TIME,time_now).commit();
        cut.start(System.currentTimeMillis());
        //Send the task Result
        SharedPreferences storageUser = this.getSharedPreferences(
                PREFERENCE_FILE_KEY_LOG_IN,
                Context.MODE_PRIVATE
        );
        String user = storageUser.getString(LOG_IN_USER, No_USER_FOUND);
        String password = storageUser.getString(LOG_IN_PASSW, "");
        Result start_result = new Result(selected_task_id, user , Result.TASK_STATE.STARTING, "");
        send_data_online(start_result,password, this);
        ((TextView)findViewById(R.id.stats)).setText("Läuft bereits für 0 s");
    }
    private void stop_work() {
        //Get tasks and send/save the task Result
        SharedPreferences storage = this.getSharedPreferences(
                PREFERENCE_ACTIVE_TASK,
                Context.MODE_PRIVATE
        );
        String task_id = storage.getString(ACTIVE_TASK, No_TASK_ID_FOUND);
        String task_name = storage.getString(ACTIVE_NAME, No_TASK_ID_FOUND);
        long task_time = storage.getLong(ACTIVE_TIME,0);
        storage.edit().putBoolean(ACTIVE_STATE, false).commit();
        SharedPreferences storageUser = this.getSharedPreferences(
                PREFERENCE_FILE_KEY_LOG_IN,
                Context.MODE_PRIVATE
        );
        String user = storageUser.getString(LOG_IN_USER, No_USER_FOUND);
        String password = storageUser.getString(LOG_IN_PASSW, "");
        Result end_result = new Result(task_id, user , Result.TASK_STATE.FINISHING, "");
        send_data_online(end_result,password, this);
        long time = (System.currentTimeMillis() - task_time)/1000;
        cut.stop();
        int hours = (int)(time/3600);
        int minutes = (int)(time/60-hours*60);
        int seconds = (int)(time-hours*3600-minutes*60);
        ((TextView)findViewById(R.id.stats)).setText("Abgeschlossene Aufgabe:\n" + task_name + " (" + task_id + ")\ndauerte: " +
            hours + " h " + minutes + " min " +
                seconds + " s"
        );
        History.save_data(this,""+task_name+ "(" + task_id +") ", hours, minutes);
        get_local_data();
        //Update Graphics
        update_spinner();
        create_display();
    }
    //Send data ########################################################################################################
    private static boolean sending = false;
    public static void send_data_online(Result res, String password, Context context) {
        SendData sender = new SendData();
        if(res != null) {
            sender.save_results_locally(res, context);
        }
        Log.d("RTCleanerinfo", "start TRy: " + sending);
        if (!sending) {
            new Thread(new Runnable() {
                public void run() {
                    sending = true;
                    boolean worked = sender.try_sending(context,password);
                    Log.d("RTCleanerinfo", "trysend worked: " + worked);
                    sending = false;
                }
            }).start();
        }
    }
    CountUpTimer cut = null;
    private class CountUpTimer {
        //Source: https://gist.github.com/MiguelLavigne/8809180c5b8fe2fc7403
        private final long interval;
        private long base;
        long counter_number = 0;
        CountUpTimer(long interval) {
            this.interval = interval;
        }
        void start(long task_time) {
            base = task_time;
            handler.sendMessage(handler.obtainMessage(MSG));
        }
        void stop() {
            handler.removeMessages(MSG);
        }
        void onTick(long elapsedTime){
            counter_number = elapsedTime/1000;
            Log.i("TIMERRR", elapsedTime+" -----------------------------");
            long time = elapsedTime/1000;
            cut.stop();
            int hours = (int)(time/3600);
            int minutes = (int)(time/60-hours*60);
            int seconds = (int)(time-hours*3600-minutes*60);
            ((TextView)findViewById(R.id.stats)).setText("Läuft bereits für " +
                    hours + " h " + minutes + " min " + seconds + " s"
            );
        }
        private static final int MSG = 1;
        private Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                synchronized (CountUpTimer.this) {
                    long elapsedTime = System.currentTimeMillis() - base;
                    onTick(elapsedTime);
                    sendMessageDelayed(obtainMessage(MSG), interval);
                }
            }
        };
    }




    //Unused
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}
    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
