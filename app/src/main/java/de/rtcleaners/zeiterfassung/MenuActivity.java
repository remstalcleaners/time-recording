package de.rtcleaners.zeiterfassung;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onclick_update_data(View v) {
        SharedPreferences storageUser = this.getSharedPreferences(
                MainActivity.PREFERENCE_FILE_KEY_LOG_IN,
                Context.MODE_PRIVATE
        );
        String user = storageUser.getString(MainActivity.LOG_IN_USER, MainActivity.No_USER_FOUND);
        String password = storageUser.getString(MainActivity.LOG_IN_PASSW, "");
        MainActivity.get_online_data(this, user, password);
    }
    public void onclick_send_data(View v) {
        SharedPreferences storageUser = this.getSharedPreferences(
                MainActivity.PREFERENCE_FILE_KEY_LOG_IN,
                Context.MODE_PRIVATE
        );
        String user = storageUser.getString(MainActivity.LOG_IN_USER, MainActivity.No_USER_FOUND);
        String password = storageUser.getString(MainActivity.LOG_IN_PASSW, "");
        MainActivity.send_data_online(null, password, this);
    }
    public void onclick_change_login(View v) {
        Intent intent = new Intent(this, LogIn.class);
        startActivity(intent);
    }
    public void onclick_view_history(View v) {
        Intent intent = new Intent(this, History.class);
        startActivity(intent);
    }
}