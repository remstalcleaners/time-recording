package de.rtcleaners.zeiterfassung;

import android.util.Log;

import java.util.HashSet;
import java.util.Set;

public class Options {
    //Task-ID*, Taskname menschlich, QRCode-SteuerungsID, Anfangszeit, Endzeit, Ort
    public Option[] options_list;
    public Options(Set<String> tasks) {
        if (tasks == null || tasks.size() < 1) {
            this.options_list = new Option[1];
            this.options_list[0] = new Option();
            return;
        }
        String[] tasksStrings = tasks.toArray(new String[tasks.size()]);
        this.options_list = new Option[tasksStrings.length+1]; //all task plus the unknown task as option
        this.options_list[0] = new Option(); //Always add unknown task first
        for (int i = 0; i < tasksStrings.length; i++) {
            if (tasksStrings[i] == null || tasksStrings[i].length() < 8) {//Empty task ",,,,,,;" is still invalid and length 7 so it must be bigger
                this.options_list[i+1] = new Option();
            } else {
                this.options_list[i+1] = new Option(tasksStrings[i]);
            }
        }
    }
    public Options() {
        this.options_list = new Option[1];
        this.options_list[0] = new Option();
    }
    /*
    public Option get_by_task_id(String id) {
        if (this.options_list == null) {
            return new Option();
        }
        for (int i = 0; i < this.options_list.length; i++) {
            if(this.options_list[i].task_id.equals(id)){
                return this.options_list[i];
            }
        }
        return new Option();
    }
    */
    public String toString() {
        String res = "";
        for(int i = 0; i < this.options_list.length; i++) {
            res += options_list[i].toString();//Assume option.toString ends with an ;
        }
        return res;
    }

    public class Option {
        String task_id;
        String task_name;
        int qr_control;
        long start_time;
        long end_time;
        int location_lat;
        int location_lon;
        Option(String str) {
            try {
                String[] s1 = str.split(",");
                try {
                    this.task_id = s1[0];
                    this.task_name = s1[1];
                } catch (Exception e) {
                    this.task_id = "FFFB";
                    this.task_name = "FFFB";
                }
                try {
                    this.qr_control = Integer.parseInt(s1[2].trim());
                    this.start_time = Long.parseLong(s1[3].trim());
                    this.end_time = Long.parseLong(s1[4].trim());
                    this.location_lat = Integer.parseInt(s1[5].trim());
                    this.location_lon = Integer.parseInt(s1[6].replace(";","").trim());
                } catch (Exception e) {
                    this.qr_control = 0;
                    this.start_time = 0;
                    this.end_time = 0;
                    this.location_lat = 0;
                    this.location_lon = 0;
                }
            } catch (Exception e) {
                this.task_id = "FFFA";
                this.task_name = "FFFA";
                this.qr_control = 0;
                this.start_time = 0;
                this.end_time = 0;
                this.location_lat = 0;
                this.location_lon = 0;
            }
        }
        Option() {
            this("UA,Unbekannte Aufgabe,0,0,0,0,0;");
        }
        public String toString() {
            return this.task_id + "," +
                    this.task_name + "," +
                    this.qr_control + "," +
                    this.start_time + "," +
                    this.end_time + "," +
                    this.location_lat + "," +
                    this.location_lon + ";";
        }
    }
}
