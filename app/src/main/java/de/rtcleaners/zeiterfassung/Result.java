package de.rtcleaners.zeiterfassung;

import android.os.SystemClock;
import android.util.Log;

public class Result {
    public enum TASK_STATE {
        STARTING,
        FINISHING
    }
    //Task-ID,Ein/Ausstempeln, Zeitpunk, Ort, QRCode-Daten
    String unique_id; //time stamp to the second + "-" + fake random number.
    String task_id;
    TASK_STATE check_in; //If false check out!
    //time is included in the unique id!
    int location_lat; //Measured latitude location when this action was performed. Using Decimal degrees (DD)
    int location_lon; //Measured longitude location when this action was performed. Using Decimal degrees (DD)
    String qr_code; //If used gives the text of the scanned qr code. Can replace task_id.

    //Initial creation of the data point of type Result.
    //All information must be available before creation, except time.
    //The time stamp is included at creation of this data structure and final.
    public Result(String task_id, String user_id, TASK_STATE check_in, String qr_code){
        //SystemClock.sleep(100);
        this.location_lat = 0; //TODO: Get GPS data
        this.location_lon = 0; //TODO: Get GPS data
        String time_stamp = String.valueOf(System.currentTimeMillis()); //Time stamp to the milli second
        //######################## !!!!!!!!!!!!!!!!!!!!!!! ############################################
        // This is the official creation point of the data. After this the data is final and must not be altered.
        // At this point time and location are recorded and all other information, not allowed to be altered.
        //######################## !!!!!!!!!!!!!!!!!!!!!!! ############################################
        this.unique_id = time_stamp + "-"+ user_id;
        this.task_id = task_id;
        this.check_in = check_in;
        this.qr_code = qr_code;
    }
    //Encode Result to a string for saving and sending over networks.
    public String toString() {
        String res = this.unique_id + "," + this.task_id + ",";
        if(this.check_in == TASK_STATE.STARTING) {
            res += "1,";
        } else {
            res += "0,";
        }
        res += this.location_lat + "," + this.location_lon + ",";
        res += this.qr_code;
        return res;
    }
    public String encode_post(String password) {
        //key1=value1&key2=value2&key3=value3
        String res = "pw=" + password + "&";
        res += "content=" + toString();
        return res;
    }
}
