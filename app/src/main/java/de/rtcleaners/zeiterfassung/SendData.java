package de.rtcleaners.zeiterfassung;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

public class SendData {
    /* Send data to the server or save is locally if not possible
    * 1. Save data locally
    * 2. Get local data
    * 3. Get Login data
    * 4. Log in
    * 5. Send data with post request
    * 6. Confirm sending
    * 7. Delete sent data locally
    * */
    private final String PREFERENCE_FILE_KEY_RESULTS = "RESULTS_STORE";
    private final String RESULTS_TEXTS = "RESULTS_TEXTS";
    private final String TO_DELETE_STORE_KEY_RESULTS = "TO_DELETE_STORE";
    private final String RESULTS_TO_DELETE = "RESULTS_TEXTS_TO_DELETE";
    public void save_results_locally(Result result, Context context) {
        SharedPreferences storage = context.getSharedPreferences(
                PREFERENCE_FILE_KEY_RESULTS,
                Context.MODE_PRIVATE
        );
        Set<String> old = storage.getStringSet(RESULTS_TEXTS, new HashSet<String>());
        HashSet<String> res = new HashSet<String>();
        for (String s: old) {
            res.add(s);
        }
        res.add(result.toString());
        Log.d("RTCleanerinfo", "Total Size::: " + res.size());
        storage.edit().putStringSet(RESULTS_TEXTS, res).commit();
    }

    public boolean try_sending(Context context, String password) {
        Log.d("RTCleanerinfo", "start TRRRYYYIINNNGG");
        delete_remembered(context);
        SharedPreferences storage = context.getSharedPreferences(
                PREFERENCE_FILE_KEY_RESULTS,
                Context.MODE_PRIVATE
        );
        Set<String> saved = storage.getStringSet(RESULTS_TEXTS, new HashSet<String>());
        //Try sending them all to server
        for (String s : saved) {
            send_string(s, context, password);
        }
        return true;
    }
    private void send_string(String result_str, Context context, String password) {
        Log.d("RTCleanerinfo", "start sending...");
        HttpURLConnection urlConnection = null;
        String response = "";
        try {
            URL url = new URL("https://rtcleaners.de/arbeit/tasks_results.php");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            //key1=value1&key2=value2&key3=value3
            writer.write("pw=" + password + "&content=" + result_str);
            //writer.write("key1=value1&key2=value2&key3=value3");
            writer.flush();
            writer.close();
            out.close();
            int responseCode = urlConnection.getResponseCode();
            Log.d("RTCleanerinfo", "" + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
                Log.d("RTCleanerinfo", "###################################");
                Log.d("RTCleanerinfo", response);
                Log.d("RTCleanerinfo", "###################################");
                if(response.equalsIgnoreCase("accepted77328")){
                    remember_delete(context, result_str);
                }
            }
            Log.d("RTCleanerinfo", "finished sending...");
        } catch (Exception e) {
            Log.d("RTCleanerinfo", e.toString());
        }
    }
    private boolean remember_delete(Context context, String str) {
        SharedPreferences storage = context.getSharedPreferences(
                TO_DELETE_STORE_KEY_RESULTS,
                Context.MODE_PRIVATE
        );
        Set<String> old = storage.getStringSet(RESULTS_TO_DELETE, new HashSet<String>());
        HashSet<String> res = new HashSet<String>();
        for (String s: old) {
            res.add(s);
        }
        res.add(str);
        Log.d("RTCleanerinfo", "Remembered to delete :" + res.size() + " Results.");
        return storage.edit().putStringSet(RESULTS_TO_DELETE, res).commit();
    }
    private boolean delete_remembered(Context context){
        SharedPreferences storage1 = context.getSharedPreferences(
                TO_DELETE_STORE_KEY_RESULTS,
                Context.MODE_PRIVATE
        );
        Set<String> to_delete = storage1.getStringSet(RESULTS_TO_DELETE, new HashSet<String>());

        SharedPreferences storage2 = context.getSharedPreferences(
                PREFERENCE_FILE_KEY_RESULTS,
                Context.MODE_PRIVATE
        );
        Set<String> old = storage2.getStringSet(RESULTS_TEXTS, new HashSet<String>());

        Set<String> filtered = new HashSet<String>();
        for (String s: old) {
            boolean contained = false;
            for (String d: to_delete){
                if(s.equals(d)){
                    contained=true;
                    break;
                }
            }
            if (!contained){
                filtered.add(s);
            }else{
                Log.d("RTCleanerinfo", "deleted: "+ s);
            }
        }
        return
                storage2.edit().putStringSet(RESULTS_TEXTS, filtered).commit() &&
                storage1.edit().putStringSet(RESULTS_TO_DELETE, new HashSet<String>()).commit();
    }
}
